# missi_phone_global-user 13 TKQ1.221114.001 V14.0.7.0.TMRMIXM release-keys
- manufacturer: xiaomi
- platform: taro
- codename: marble
- flavor: missi_phone_global-user
- release: 13
- id: TKQ1.221114.001
- incremental: V14.0.7.0.TMRMIXM
- tags: release-keys
- fingerprint: Xiaomi/marble/marble:13/SKQ1.221022.001/V14.0.7.0.TMRMIXM:user/release-keys
POCO/marble_global/marble:13/SKQ1.221022.001/V14.0.7.0.TMRMIXM:user/release-keys
POCO/marblein/marblein:13/SKQ1.221022.001/V14.0.7.0.TMRMIXM:user/release-keys
- is_ab: true
- brand: POCO
- branch: missi_phone_global-user-13-TKQ1.221114.001-V14.0.7.0.TMRMIXM-release-keys
- repo: poco_marble_dump
